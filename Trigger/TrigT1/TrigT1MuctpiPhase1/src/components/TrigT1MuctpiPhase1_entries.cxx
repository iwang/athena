#include "../../TrigT1MuctpiPhase1/MUCTPI_AthAlg.h"
#include "../../TrigT1MuctpiPhase1/MUCTPI_AthTool.h"
#include "../../TrigT1MuctpiPhase1/TrigThresholdDecisionTool.h"

DECLARE_COMPONENT( LVL1MUCTPIPHASE1::MUCTPI_AthAlg )
DECLARE_COMPONENT( LVL1MUCTPIPHASE1::MUCTPI_AthTool )
DECLARE_COMPONENT( LVL1::TrigThresholdDecisionTool )
